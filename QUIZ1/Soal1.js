import React, { Component } from 'react'
import { Text, View } from 'react-native'

export default class FComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: 'Jhon Doe'
        }
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({
                name: 'Asep'
            })
        }, 3000)
    }

    render() {
        return (
            <View>
                <Text>{this.state.name}</Text>
            </View>
        )
    }
}
